import random
import math
import gym
import csv
import gym_DQN_jobshop50
import numpy as np
import torch
import tqdm
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import matplotlib.pyplot as plt
from torchrl.data import ReplayBuffer
from tensordict import TensorDict
from torchrl.data.replay_buffers.storages import LazyMemmapStorage

class model(nn.Module):          
    def __init__(self):
        super(model,self).__init__()   
        self.layer1 = nn.Linear(245,200) #Tanh
        self.layer2 = nn.Linear(200,100)
        self.layer3 = nn.Linear(100,100)
        self.layer4 = nn.Linear(100,7) #fc2

    def forward(self,x):
        x = nn.Tanh()(self.layer1(x))   #instead use nn.Tanh()
        x = nn.Tanh()(self.layer2(x))   #makes more sense for negative rews
        x = nn.Tanh()(self.layer3(x))
        return self.layer4(x) #x

def list2tensor(x):
    return torch.tensor(x).float()    
def tensor2list(a):
    return [int(x) for x in a.tolist()]

env = gym.make('process-dqnjob50')
policy = model()
policy.load_state_dict(torch.load("data_DQN_jobshop50steps/rnn_agent_0.pth"))  #Loading agent
target_net = model()
target_net.load_state_dict(policy.state_dict())
#target_net.eval()
#optimizer = optim.RMSprop(policy.parameters())
optimizer = optim.Adam(policy.parameters(), lr = 1e-3)  # 0.0001
criterion = F.cross_entropy #torch.nn.MSELoss() #F.smooth_l1_loss #F.kl_div

memory = 500         #Size of the replay buffer
storage = LazyMemmapStorage(memory,scratch_dir='/scratch/')
bufferx = ReplayBuffer(collate_fn = lambda x:x, storage=storage)
gamma = 0.7
EPS_START = 0.8      #00.6
EPS_END = -0.2
EPS_DECAY = 95000   
WINDOW = 30

def addEpisode(prev,curr,act,reward):
    tens_dict = TensorDict({'prev':prev[0],'curr':curr[0],'action':act[0],'reward':reward[0]},batch_size=[50])
    bufferx.extend(tens_dict)

def trainNet(total_episodes):
    if total_episodes == 0:
        return
    sample = bufferx.sample(25)

    inp = sample.get('prev')   
    target = sample.get('curr')
    actions = sample.get('action')  
    rew = sample.get('reward')  

    targets = target.reshape(-1,245)      #-1, num_seq, input_len [[[],[],[]], [[],[],[]], ...]
    hs = targets.shape[0]                 #number of steps registered

    ccs = inp.reshape(-1,245)             # -1, num_seq, input_len [[[],[],[]], [[],[],[]], ...]
    hs1 = ccs.shape[0]                    #number of steps registered

    qvals  = target_net(targets)    #we get the Q vals of s', from the target net

    #actions = actions.type('torch.LongTensor') 
    actions = actions.reshape(-1,1)      #make a tensor of actions (argmax)(long of the episode:150)
    inps = policy(ccs).gather(1,actions)  #get the Q vals of s from the policy using the actions as indexes
    p1,p2 = qvals.detach().max(1)         # max() outputs values and indices of max values
    targ = torch.Tensor(1,p1.shape[0])    #tensor with the number of qvalues (max) in p1

    #the individual qvalues of the target = reward + gamma * maxQval(s') 
    p1[-1] = 0
    targ[0] = rew + gamma*p1              #if we are in any other step
    #target[0][-1] = rew[-1]              # if we are in the last step of the episode 

    optimizer.zero_grad()
    inps = inps.reshape(1,-1)           #we reshape qvalues(s) into (1,steps)
    loss = criterion(inps,targ)
    loss.backward()
    for param in policy.parameters():
        param.grad.data.clamp(-1,1)      #clippiing values in the gradients
    optimizer.step()

def trainDQN(episodes):
    max_steps = 50
    steps_done = 0
    global store
    eps_threshold = EPS_START
    #testRs = []
    #testDemand = []
    #proc_tim = []
    #action_list = []
    #demand_list = []
    #epsilon = []
    pbar = tqdm.trange(episodes) #new
    for epi in pbar:  
        prev = torch.zeros([245])
        p_time = torch.tensor([[[1,2,0,0,0,3,2],[1,0,2,0,0,3,2],[0,2,0,2,0,0,1],
                                [2,0,0,0,2,0,2],[2,0,2,2,0,0,1],[1,2,0,0,0,0,1]],
                               [[1,2,0,0,0,2,2],[1,0,2,0,0,2,2],[0,2,0,2,0,0,1],
                                [2,0,0,0,2,0,2],[2,0,2,2,0,0,1],[1,2,0,0,0,0,1]],
                               [[1,2,0,0,0,1,2],[1,0,2,0,0,1,2],[0,2,0,2,0,0,1],
                                [2,0,0,0,2,0,2],[2,0,2,2,0,0,1],[1,2,0,0,0,0,1]]])

        p_demand = torch.tensor([[12,9,10,9],[10,11,11,4],[10,12,10,11],[10,12,4,11]])
        a = random.choice([0,1,2])
        #a = 0
        times = p_time[a]
        #proc_tim.append(a)
        #d1 = p_demand[random.choice([0,1,2,3])]    #0,1,2,3,4
        #d2 = p_demand[random.choice([0,1,2,3])]    #0,1,2,3,4
        #d3 = p_demand[random.choice([0,1,2,3])]    #0,1,2,3,4
        d1 = p_demand[0]
        d2 = p_demand[1]
        d3 = p_demand[2]
        d_i = torch.tensor([8,8,8,8])
        d_sum = sum((d1,d2,d3,d_i))
        #demand_list.append([d1,d2,d3])
        intimes = torch.zeros(WINDOW, dtype=torch.int64)    #number of time steps
        done = False
        steps = 0
        rew = 0
        ol = 0
        w = 0
        s  = []
        s_ = []
        a  = []
        r  = []
        while steps < max_steps: #done == False:
            if steps == 0:
                prev[-5:-1] = d_i                    
            elif steps == int(max_steps/5):
                prev[-5:-1] += d1
            elif steps == int(max_steps*2/5):
                prev[-5:-1] += d2
            elif steps == int(max_steps*3/5):
                prev[-5:-1] += d3

            eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * steps_done / EPS_DECAY)
            steps += 1 
            output = policy(prev)  

            rand = random.uniform(0,1)
            if rand < eps_threshold:       #EPSILON
                action = random.choice([0,1,2,3,4,5,6])
            else:                          #GREEDY   
                h = prev[-5:-1]           #select action only from non processed products 
                if torch.all(h<0):
                    action = output.argmax().item() 
                else:
                    j = [i for i,v in enumerate(h) if v > 0]  #products that already started
                    l = [i + 1 for i in j]
                    k = [0]           #always 0 action
                    if 1 in l:
                        k.append(1)
                        k.append(2)
                    if 2 in l:
                        k.append(3)
                        k.append(4)
                    if 3 in l:
                        k.append(5)
                    if 4 in l:
                        k.append(6)
                    ix = output[k].argmax().item()
                    action = k[ix]   

            #action_list.append(action)
            prev_o = torch.cat((prev[8:-5], torch.zeros((8), dtype=int),prev[-5:]))
            prev_os = tensor2list(prev_o)
            intimes = np.append(intimes[1:],(action))
            obs, reward, done, intimes, ovl1, ovl2 = env.step1(action, prev_os, intimes, times) 
            rew = rew + reward
            ol = ol + ovl1
            w = w + ovl2
            obs = list2tensor(obs)
            prev = obs

            #to the replay buffer
            s.append(prev)
            s_.append(obs)
            a.append(action)
            r.append(reward)

            #train policy
            trainNet(epi) #at every time step the net is updated

            #update window
            steps_done += 1

        s  = torch.unsqueeze(torch.stack(s), dim=0)
        s_ = torch.unsqueeze(torch.stack(s_),dim=0)
        a  = torch.unsqueeze(torch.tensor(a),dim=0)
        r  = torch.unsqueeze(torch.tensor(r,dtype=torch.float),dim=0)
        addEpisode(s,s_,a,r)  

        #testRs += [rew]
        dleft = sum(obs[-5:-1])
        d_sum = sum(d_sum)
        dperc = torch.div(dleft,d_sum)*100
        dperc = dperc.numpy()
        #testDemand.append(dperc) #[(sum(obs[-1][-5:-1])).item()]
        #epsilon += [eps_threshold]
        #f = open('data_DQN_jobshop50steps/dems_rews_ovs','a')
        #writer = csv.writer(f,lineterminator='\n')
        #writer.writerow([rew,dperc,ol,w])
        #f.close()
        
        #Early stopping
        #if epi > 3000:
        #    if testRs[-5:] > [80,80,80,80,80]:
        #        break
        #update target network
        if epi % 10 == 0:
            target_net.load_state_dict(policy.state_dict())
        pbar.set_description("Episode counter") 

    pbar.close()
    #return testRs, testDemand, epsilon, action_list, proc_tim, demand_list